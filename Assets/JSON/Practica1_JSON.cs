﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using SQLite4Unity3d;
using System.Collections.Generic;
using SimpleJSON;

public class Practica1_JSON : MonoBehaviour {

    private static string jsonfile = "Practica_1.json";


	// Use this for initialization
	void Start () {

        // Miramos si existe nuestra BD en persistend data base
        string filepath = Application.persistentDataPath + "/" + jsonfile;
        if (!File.Exists(filepath))
        {
            //Copiamos la base de datos de streamingAssets a persistendDatabase
            #if UNITY_ANDROID 
				            WWW loadDb = new WWW(Application.streamingAssetsPath +"/"+ databasefile);  // this is the path to your StreamingAssets in android
				            while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
				            // then save to Application.persistentDataPath
				            File.WriteAllBytes(filepath, loadDb.bytes);
            #else
                        string appdb = Application.streamingAssetsPath + "/" + jsonfile;  // this is the path to your StreamingAssets in iOS
                        // then save to Application.persistentDataPath
                        File.Copy(appdb, filepath);
            #endif
        }

        //Ya tenemos nuestra bd en su url
        Debug.Log("JSON File: " + filepath);       
 
        //ACABAR con el SimpleJSON !!!!!!!!

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
