﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using SQLite4Unity3d;
using System.Collections.Generic;
using SimpleJSON;

public class Practica_3_JSON : MonoBehaviour {

    public class HighscoreUser
    {    
        public string name { get; set; }
        public int highscore { get; set; }
    }

    private List<HighscoreUser> hsu;

    public List<HighscoreUser> getHighscores()
    {
        //ACABAR

        return hsu;
    }
    
    public void addHighscore(string name, int value){
        
    }


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public string ReadFilePath(string path)
    {
        // Miramos si existe nuestra BD en persistend data base
        string filepath = Application.persistentDataPath + "/" + path;
        if (!File.Exists(filepath))
        {
            //Copiamos la base de datos de streamingAssets a persistendDatabase
            #if UNITY_ANDROID 
				        WWW loadDb = new WWW(Application.streamingAssetsPath +"/"+ databasefile);  // this is the path to your StreamingAssets in android
				        while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
				        // then save to Application.persistentDataPath
				        File.WriteAllBytes(filepath, loadDb.bytes);
            #else
                        string appdb = Application.streamingAssetsPath + "/" + path;  // this is the path to your StreamingAssets in iOS
                        // then save to Application.persistentDataPath
                        File.Copy(appdb, filepath);
            #endif
        }

        //Ya tenemos nuestra bd en su url
        Debug.Log("JSON File: " + filepath);

        return filepath;
    }
}
